# pylint: disable=W
import time
from http.server import BaseHTTPRequestHandler, HTTPServer

HOST_NAME = '0.0.0.0'
PORT_NUMBER = 8000
AUTH_KEY = 8651078684

arrayOfNonRegUsers = {}


def addUser(username):
    if username not in arrayOfNonRegUsers.keys():
        arrayOfNonRegUsers.update({username: 3})


def howManyClicksLeft(username):
    for key, value in arrayOfNonRegUsers.items():
        if key == username:
            arrayOfNonRegUsers[username] -= 1
    return arrayOfNonRegUsers[username]


def getCalc(command):
    operation, num1, num2 = command.split(',')
    try:
        num1 = float(num1)
        num2 = float(num2)
    except ValueError:
        return 'error'
    if operation == 'add':
        return num1 + num2
    elif operation == 'sub':
        return num1 - num2
    elif operation == 'mul':
        return num1*num2
    elif operation == 'div' and num2 != 0:
        return num1/num2
    elif num2 == 0:
        return 'nan'
    return 'error'


def notReg(username, command):
    addUser(username)
    remaining = howManyClicksLeft(username)
    if remaining >= 0:
        return '{"response": "%s", "data": "%s"}' % (remaining, getCalc(command))
    else:
        return '{"response":"Please LOG IN"}'


def makeNewUser(username, password):
    fh = open("{}.txt".format(username), "x")
    fh. write("{},{}\n".format(username, password))
    fh.close()


def userExists(username):
    try:
        fh = open("{}.txt".format(username), 'r')
        fh.close()
        return True
    except FileNotFoundError:
        return False


def saveUser(username, password):
    if not userExists(username):
        makeNewUser(username, password)
        return 'OK'
    else:
        return 'BAD'


def loginUser(username, password):
    if userExists(username):
        with open('{}.txt'.format(username)) as f:
            first_line = f.readline()
            user, pw = first_line.split(',')
            user = user.strip()
            pw = pw.strip()
            if pw == password:
                return 'OK'
            else:
                return 'BAD'
    else:
        return 'BAD'


def getHisotry(username):
    if userExists(username):
        fomedJson = ''
        with open('{}.txt'.format(username)) as fp:
            line = fp.readline()
            line = fp.readline()
            while line:
                oper, num1, num2 = line.split(',')
                num2 = num2.strip('\n')
                fomedJson += '{"oper":"%s","num1":"%s","num2":"%s"},' % (
                    oper, num1, num2)
                line = fp.readline()
            return fomedJson[:-1]
    else:
        return 'BAD'


def saveCalc(username, command):
    fh = open("{}.txt".format(username), 'a')
    fh.write(command+'\n')


def regUser(username, password, command):
    if command == 'register':
        return '{"response": "%s"}' % saveUser(username, password)
    if command == 'login':
        return '{"response": "%s"}' % loginUser(username, password)
    if command == 'history':
        return '{"response": [%s], "type": "history"}' % getHisotry(username)
    if ',' in command:
        saveCalc(username, command)
        return '{"response": "1", "data": "%s"}' % getCalc(command)


class MyHandler(BaseHTTPRequestHandler):

    def authenticate(self):
        auth = self.path[1:11]
        try:
            int(auth)
        except ValueError:
            return False
        if(int(auth) != AUTH_KEY):
            return False
        return True

    # returns whats being display
    def http_querry(self):
        if not self.authenticate():
            return 'Please use valid API key'
        querry = self.path[12:]
        if querry == 'connection':
            return '{"response":"connected"}'
        passedArguments = querry.split('/')
        username = passedArguments[0].strip()
        pw = passedArguments[1].strip()
        command = passedArguments[2].strip()
        if pw == 'null':
            return notReg(username, command)
        else:
            return regUser(username, pw, command)
        return 'Something went wrong'

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()

    def do_GET(self):
        if self.path == '/favicon.ico':
            self.respond(404)
        self.respond(200)

    def handle_http(self, status_code, render):
        self.send_response(status_code)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        content = '''{}'''.format(render)
        return bytes(content, 'UTF-8')

    def respond(self, status):
        response = self.handle_http(status, self.http_querry())
        self.wfile.write(response)


if __name__ == '__main__':
    server_class = HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime(), 'Server Starts - %s:%s' % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), 'Server Stops - %s:%s' % (HOST_NAME, PORT_NUMBER))
